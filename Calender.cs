﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Calender : Form
    {
        HashSet<DateTime> birthdays;
        public Calender()
        {
            InitializeComponent();
            string path = @"C: \Users\magshimim\Documents\Magshimim\advanced_Programming\l6\WindowsFormsApp1\WindowsFormsApp1\MagshimDB.txt";
            string[] pair = new string[2];
            string[] time = new string[3];

            if (File.Exists(path))
            {
                string[] info = System.IO.File.ReadAllLines(path);
                foreach (string element in info)
                {
                    pair = element.Split(',');
                    time = pair[1].Split('/');
                    birthdays.Add(new DateTime(int.Parse(time[2]), int.Parse(time[1]), int.Parse(time[0])));
                }
            }
            try
            {
                foreach (DateTime element in birthdays)
                {
                    monthCalendar1.AddBoldedDate(element);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Calender_Load(object sender, EventArgs e)
        {

        }
    }
}
